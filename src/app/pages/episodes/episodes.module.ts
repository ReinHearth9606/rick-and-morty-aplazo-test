import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpisodesRoutingModule } from './episodes-routing.module';

import { EpisodesComponent } from './episodes.component';

import { CardsContainerModule } from '@components/cards-container/cards-container.module';

@NgModule({
  declarations: [EpisodesComponent],
  imports: [CommonModule, EpisodesRoutingModule, CardsContainerModule],
})
export class EpisodesModule {}
