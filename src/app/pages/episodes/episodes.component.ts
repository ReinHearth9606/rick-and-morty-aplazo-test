import { Component, OnInit } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { loadEpisodes } from '@actions/episode.actions';

import { episode } from '@interfaces/episode.interface';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss'],
})
export class EpisodesComponent implements OnInit {
  public episodes: Array<episode> = [];
  public pagination: Array<{ value: number; next: Array<any> }> = [];

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(loadEpisodes({ page: '?pages=1&name=' }));
    this.store.pipe(select((state: any) => state.episodes)).subscribe((epi) => {
      (this.episodes = epi.items),
        (this.pagination = Array(epi.info.pages)
          .fill(0)
          .map((x, i) => ({
            value: i + 1,
            next: epi.info.next
              ? epi.info.next
                  .split('?')[1]
                  .split('&')
                  .map((ele: string) => {
                    let result = ele.split('=')[0];
                    if (ele.includes('name')) {
                      result = ele;
                    }
                    return result;
                  })
              : [],
          })));
    });
  }
}
