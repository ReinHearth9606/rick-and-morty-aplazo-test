import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersRoutingModule } from './characters-routing.module';

import { CharactersComponent } from './characters.component';

import { CardsContainerModule } from '@components/cards-container/cards-container.module';
import { DetailsComponent } from '@components/details/details.component';

@NgModule({
  declarations: [CharactersComponent, DetailsComponent],
  imports: [CommonModule, CharactersRoutingModule, CardsContainerModule],
})
export class CharactersModule {}
