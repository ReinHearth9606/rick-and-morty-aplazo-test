import { Component, OnInit } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { info } from '@interfaces/store.interface';
import { character } from '@interfaces/characters.interface';

import { loadEpisodes } from '@actions/episode.actions';
import { loadLocations } from '@actions/location.actions';
import { loadCharacters } from '@actions/characters.actions';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss'],
})
export class CharactersComponent implements OnInit {
  public characters: Array<character> = [];
  public pagination: Array<{ value: number; next: Array<any> }> = [];

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.callStore();
  }

  private callStore(): void {
    this.store.dispatch(loadCharacters({ page: '?pages=1&name=' }));
    this.store.dispatch(loadEpisodes({ page: '?pages=1&name=' }));
    this.store
      .pipe(select((state: any) => state.characters))
      .subscribe((char) => {
        (this.characters = char.items),
          (this.pagination = Array(char.info.pages)
            .fill(0)
            .map((x, i) => ({
              value: i + 1,
              next: char.info.next
                ? char.info.next
                    .split('?')[1]
                    .split('&')
                    .map((ele: string) => {
                      let result = ele.split('=')[0];
                      if (ele.includes('name')) {
                        result = ele;
                      }
                      return result;
                    })
                : [],
            })));
      });
  }
}
