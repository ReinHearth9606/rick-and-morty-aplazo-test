import { Component, OnInit } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { location } from '@interfaces/location.interface';

import { loadLocations } from '@actions/location.actions';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  public locations: Array<location> = [];
  public pagination: Array<{ value: number; next: Array<any> }> = [];

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(loadLocations({ page: '?pages=1&name=' }));
    this.store
      .pipe(select((state: any) => state.locations))
      .subscribe((loc) => {
        (this.locations = loc.items),
          (this.pagination = Array(loc.info.pages)
            .fill(0)
            .map((x, i) => ({
              value: i + 1,
              next: loc.info.next
                ? loc.info.next
                    .split('?')[1]
                    .split('&')
                    .map((ele: string) => {
                      let result = ele.split('=')[0];
                      if (ele.includes('name')) {
                        result = ele;
                      }
                      return result;
                    })
                : [],
            })));
      });
  }
}
