import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationsRoutingModule } from './locations-routing.module';

import { LocationsComponent } from './locations.component';

import { CardsContainerModule } from '@components/cards-container/cards-container.module';

@NgModule({
  declarations: [LocationsComponent],
  imports: [CommonModule, LocationsRoutingModule, CardsContainerModule],
})
export class LocationsModule {}
