import { Component } from '@angular/core';

import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { showCharacters } from '@actions/characters.actions';
import { showEpisodes } from '@actions/episode.actions';
import { showLocation } from '@actions/location.actions';

import { NAV_BTNS } from '@constants/navbar.constants';

import { NavBtn } from '@interfaces/navbar.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  public navBtns: Array<NavBtn> = NAV_BTNS;

  constructor(private store: Store, private router: Router) {}

  public filterArrays(strFilter: string): void {
    switch (location.pathname) {
      case '/characters':
        return this.store.dispatch(
          showCharacters({
            query:
              location.pathname.slice(1, location.pathname.length - 1) +
              '/?name=' +
              strFilter,
          })
        );
      case '/episodes':
        return this.store.dispatch(
          showEpisodes({
            query:
              location.pathname.slice(1, location.pathname.length - 1) +
              '/?name=' +
              strFilter,
          })
        );
      case '/locations':
        return this.store.dispatch(
          showLocation({
            query:
              location.pathname.slice(1, location.pathname.length - 1) +
              '/?name=' +
              strFilter,
          })
        );
    }
  }

  public goToRoute(url: string): void {
    this.router.navigate([url]);
  }
}
