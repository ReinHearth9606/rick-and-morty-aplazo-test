import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { CharactersService } from '@services/characters.service';

import { character } from '@interfaces/characters.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() img: string | undefined;
  @Input() name: string | undefined;
  @Input() status: string | undefined;
  @Input() species: string | undefined;
  @Input() gender: string | undefined;
  @Input() air_date: string | undefined;
  @Input() episode: string | undefined;
  @Input() characters: Array<string> = [];
  @Input() created: string | undefined;
  @Input() id: number = 0;
  @Input() typeLocation: string | undefined;
  @Input() dimension: string | undefined;
  @Input() type: 'character' | 'episode' | 'location' | '' = '';

  public isVisible: boolean = false;
  private query: string = '';
  public listCharacters: Array<character> = [];

  constructor(
    private router: Router,
    private charactersServices: CharactersService
  ) {}

  ngOnInit(): void {
    if (this.type !== 'character') {
      this.query = this.characters
        .map((c: string) => c.split('character/')[1])
        .join(',');
      this.charactersServices
        .getMultiCharacters(this.query)
        .subscribe((res: Array<character> | character) => {
          this.listCharacters = Array.isArray(res) ? res : [res];
        });
    }
  }

  public getDetails(id: number): void {
    if (this.type === 'character') {
      this.router.navigate(['/characters/details', id]);
    }
  }

  public changeVisible(): void {
    if (!this.isVisible) {
      setTimeout(() => (this.isVisible ? this.changeVisible() : null), 6000);
    }
    this.isVisible = !this.isVisible;
  }
}
