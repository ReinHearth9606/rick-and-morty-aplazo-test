import { Component, Input, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { character } from '@interfaces/characters.interface';
import { episode } from '@interfaces/episode.interface';
import { location } from '@interfaces/location.interface';

import { loadCharacters } from '@actions/characters.actions';
import { loadEpisodes } from '@actions/episode.actions';
import { loadLocations } from '@actions/location.actions';

@Component({
  selector: 'app-cards-container',
  templateUrl: './cards-container.component.html',
  styleUrls: ['./cards-container.component.scss'],
})
export class CardsContainerComponent implements OnInit {
  @Input() items: Array<character | episode | location | any> = [];
  @Input() type: 'character' | 'episode' | 'location' | '' = '';
  @Input() pagination: Array<{ value: number; next: Array<any> }> = [];

  constructor(private store: Store) {}

  ngOnInit(): void {}

  public dispatchPage(target: any, next: any[]): void {
    let query = next.length
      ? next.length > 1
        ? next[0] + '=' + target.value + '&' + next[1]
        : next[0] + '=' + target.value
      : 'page=' + target.value;

    switch (location.pathname) {
      case '/characters':
        return this.store.dispatch(loadCharacters({ page: '?' + query }));
      case '/locations':
        return this.store.dispatch(loadLocations({ page: '?' + query }));
      case '/episodes':
        return this.store.dispatch(loadEpisodes({ page: '?' + query }));
      default:
        return;
    }
  }
}
