import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsContainerComponent } from './cards-container.component';

import { CardModule } from '@components/card/card.module';

@NgModule({
  declarations: [CardsContainerComponent],
  imports: [CommonModule, CardModule],
  exports: [CardsContainerComponent],
})
export class CardsContainerModule {}
