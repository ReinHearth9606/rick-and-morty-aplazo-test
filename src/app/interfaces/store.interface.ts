import { characterState } from "./characters.interface";
import { episodeState } from "./episode.interface";
import { locationState } from "./location.interface";

export interface store {
  characters: characterState;
  locations: locationState;
  episode: episodeState;
}

export interface info {
  count: number;
  pages: number;
  next: string | null;
  prev: string | null;
}

export interface response {
  info: info
  results:any[]
}