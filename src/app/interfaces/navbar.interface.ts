export interface NavBtn {
  title: string;
  route: string;
}
