import { info } from "./store.interface"

export interface episode{
        id: number,
        name: string,
        air_date: string,
        episode: string,
        characters: Array<string>,
        url: string,
        created: string
}

export interface episodeState{
    items: Array<episode>,
    info: {}
}

export interface responseEpisodes{
    info:info,
    results: Array<episode>
}