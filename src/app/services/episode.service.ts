import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { episode, responseEpisodes } from '@interfaces/episode.interface';

import { apiResources } from 'src/app/api-resources';

import { environment } from '@environments/environment';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EpisodeService {
  constructor(private http: HttpClient) {}

  public getEpisodes(page: string): Observable<responseEpisodes> {
    return this.http.get<responseEpisodes>(
      environment.BASE_URL + apiResources.getEpisodes + page
    );
  }

  public getMultiEpisodes(query: string): Observable<episode[]> {
    return this.http.get<Array<episode>>(
      environment.BASE_URL + apiResources.getEpisodes + query
    );
  }
}
