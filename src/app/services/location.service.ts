import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

import { apiResources } from 'src/app/api-resources';

import { responseLocations } from '@interfaces/location.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocationService {
  constructor(private http: HttpClient) {}

  public getLocations(page: string): Observable<responseLocations> {
    return this.http.get<responseLocations>(
      environment.BASE_URL + apiResources.getLocation + page
    );
  }
}
