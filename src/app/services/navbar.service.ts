import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { apiResources } from 'src/app/api-resources';

import { response } from '@interfaces/store.interface';
import { responseCharacters } from '@interfaces/characters.interface';
import { responseEpisodes } from '@interfaces/episode.interface';
import { responseLocations } from '@interfaces/location.interface';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavbarService {
  constructor(private http: HttpClient) {}

  public filterItems(filterQuery: string): Observable<response> {
    return this.http.get<response>(environment.BASE_URL + filterQuery);
  }
}
