import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

import { apiResources } from 'src/app/api-resources';

import {
  character,
  responseCharacters,
} from '@interfaces/characters.interface';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private http: HttpClient) {}

  public getCharacters(page: string): Observable<responseCharacters> {
    return this.http.get<responseCharacters>(
      environment.BASE_URL + apiResources.getCharacter + page
    );
  }

  public getMultiCharacters(query: string): Observable<character[]> {
    return this.http.get<Array<character>>(
      environment.BASE_URL + apiResources.getCharacter + query
    );
  }
}
