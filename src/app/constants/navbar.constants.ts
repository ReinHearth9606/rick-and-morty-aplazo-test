export const NAV_BTNS = [
  {
    title: 'Characters',
    route: '/characters',
  },
  {
    title: 'Locations',
    route: '/locations',
  },
  {
    title: 'Episodes',
    route: '/episodes',
  },
];
