import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NavbarModule } from '@components/navbar/navbar.module';

import { CharacterEffects } from '@effects/characters.effects';
import { EpisodeEffects } from '@effects/episode.effects';
import { LocationEffects } from '@effects/location.effects';

import { characterReducer } from '@reducer/characters.reducer';
import { episodeReducer } from '@reducer/episode.reducer';
import { locationReducer } from '@reducer/location.reducer';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NavbarModule,
    HttpClientModule,
    StoreModule.forRoot(
      {
        episodes: episodeReducer,
        characters: characterReducer,
        locations: locationReducer,
      },
      {}
    ),
    EffectsModule.forRoot([EpisodeEffects, CharacterEffects, LocationEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
